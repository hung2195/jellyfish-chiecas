/* change image pc to sp
-------------------------------------------------------------------------------------------------------------------- */
{
  jQuery(document).ready(function ($) {
    // 置換の対象とするclass属性。
    // var $elem = $(".switch");
    // // 置換の対象とするsrc属性の末尾の文字列。
    // var sp = "_sp.";
    // var pc = "_pc.";
    // // 画像を切り替えるウィンドウサイズ。
    // var replaceWidth = 960;
    //
    // function imageSwitch() {
    //   // ウィンドウサイズを取得する。
    //   var windowWidth = parseInt(window.innerWidth);
    //   // ページ内にあるすべての`.js-image-switch`に適応される。
    //   $elem.each(function () {
    //     var $this = $(this);
    //     // ウィンドウサイズが750px以上であれば_spを_pcに置換する。
    //     if (windowWidth >= replaceWidth) {
    //       $this.attr("src", $this.attr("src").replace(sp, pc));
    //       // ウィンドウサイズが750px未満であれば_pcを_spに置換する。
    //     } else {
    //       $this.attr("src", $this.attr("src").replace(pc, sp));
    //     }
    //   });
    // }
    // imageSwitch();
    // // 動的なリサイズは操作後0.2秒経ってから処理を実行する。
    // var resizeTimer;
    // $(window).on("resize", function () {
    //   clearTimeout(resizeTimer);
    //   resizeTimer = setTimeout(function () {
    //     imageSwitch();
    //   }, 200);
    // });
    // setTimeout(() => {
    //   $(".loading").fadeOut("slow");
    // }, 400);
  });
}
/*  hide scroll bar
-------------------------------------------------------------------------------------------------------------------- */
{
  jQuery(document).ready(function ($) {
    let errorPage = $("#error-page");
    if (errorPage.length > 0) {
      $(".scrollDown").addClass(" cs_hide");
    }
  });
}

/*  hide scroll
-------------------------------------------------------------------------------------------------------------------- */
{
  jQuery(document).ready(function ($) {
    $(window).on("load", function (event) {
      let scroll = $(window).scrollTop();
      if (scroll > 100) {
        $(".scrollDown").addClass("hide");
      } else {
        $(".scrollDown").removeClass("hide");
      }
    });
    function hideScrollBtn() {
      let scroll = $(window).scrollTop();
      if (scroll > 100) {
        $(".scrollDown").addClass("hide");
      } else {
        $(".scrollDown").removeClass("hide");
      }
    }
    hideScrollBtn();
    var scrollTimer;
    $(window).on("scroll", function () {
      clearTimeout(scrollTimer);
      scrollTimer = setTimeout(function () {
        hideScrollBtn();
      }, 16.67);
    });
  });
}
/*  slide TOP
-------------------------------------------------------------------------------------------------------------------- */
{
  let js_top_swiper = $(".js_top_swiper");
  if (js_top_swiper.length > 0) {
    var swiper = new Swiper(".js_top_swiper", {
      effect: "fade",
      loop: true,
      loopedSlides: 1,
      speed: 1200,
      autoplay: {
        delay: 1800,
        disableOnInteraction: false,
        pauseOnMouseEnter: false,
      },
      // autoplay: false,
      // allowTouchMove: false,
    });
  }
}
/*  slide TOP
-------------------------------------------------------------------------------------------------------------------- */
{
  let js_page_swiper = $(".js_page_swiper");
  if (js_page_swiper.length > 0) {
    var swiper = new Swiper(".js_page_swiper", {
      effect: "fade",
      loop: true,
      loopedSlides: 1,
      speed: 1200,
      autoplay: {
        delay: 1800,
        disableOnInteraction: false,
        pauseOnMouseEnter: false,
      },
      // autoplay: false,
      allowTouchMove: false,
    });
  }
}

/*  slide picup
-------------------------------------------------------------------------------------------------------------------- */
{
  let js_top_swiper = $(".js_picUp_swiper");
  if (js_top_swiper.length > 0) {
    var swiper = new Swiper(".js_picUp_swiper", {
      loop: true,
      loopedSlides: 1,
      speed: 700,
      autoplay: {
        delay: 3000,
        disableOnInteraction: false,
        pauseOnMouseEnter: false,
      },
      navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
      },
      pagination: {
        el: ".swiper-pagination",
        clickable: true,
      },
      autoplay: false,
      // allowTouchMove: false,
    });
  }
}

/*  menu fixed
-------------------------------------------------------------------------------------------------------------------- */
{
  jQuery(document).ready(function ($) {
    let gnavi = $(".gnavi");
    let overlay = $(".overlay");
    let menuBtn = $("#menuBtn");
    menuBtn.on("click", function () {
      gnavi.toggleClass("open");
      overlay.toggleClass("open");
      $(this).toggleClass("open");
      if ($(this).find(".inner_name").text() == "MENU") {
        $(this).find(".inner_name").text("CLOSE");
      } else {
        $(this).find(".inner_name").text("MENU");
      }
    });
    overlay.on("click", function () {
      gnavi.toggleClass("open");
      $(this).toggleClass("open");
      menuBtn.toggleClass("open");
      if (menuBtn.find(".inner_name").text() == "MENU") {
        menuBtn.find(".inner_name").text("CLOSE");
      } else {
        menuBtn.find(".inner_name").text("MENU");
      }
    });
  });
}
/* scroll effect
------------------------------------------ */
jQuery(document).ready(function ($) {
  function scrollAnime() {
    $(".u_anime-fadeUp").each(function () {
      var elemPos = $(this).offset().top;
      var scroll = $(window).scrollTop();
      var windowHeight = $(window).height();
      if (scroll > elemPos - windowHeight + 60) {
        $(this).addClass("show");
      }
    });
  }
  scrollAnime();
  var scrollTimer;
  $(window).on("scroll", function () {
    clearTimeout(scrollTimer);
    scrollTimer = setTimeout(function () {
      scrollAnime();
    }, 16.67);
  });
});
/* scroll effect
------------------------------------------ */
jQuery(document).ready(function ($) {
  function scrollMv() {
    $(".js-plx").each(function () {
      var scroll = $(window).scrollTop();
      var windowHeight = $(window).height();
      if (scroll > windowHeight + 300) {
        $(this).addClass("show");
      } else {
        $(this).removeClass("show");
      }
    });
  }
  scrollMv();
  var scrollTimer;
  $(window).on("scroll", function () {
    clearTimeout(scrollTimer);
    scrollTimer = setTimeout(function () {
      scrollMv();
    }, 16.67);
  });
});
/* set bg fixed ios footer
-------------------------------------------------------------------------------------------------------------------- */
{
  jQuery(document).ready(function ($) {
    let contents = $(".logoft");
    let footer = $(".footer");
    let fixed = $(".logoft");
    let fixedChild = $(".logoft .plx-img");
    let pointCheck = 200;
    if ($(window).width() < 425) {
      pointCheck = 150;
    }
    function checkfixed() {
      if (footer.length > 0) {
        var fixed_position = fixedChild.offset().top;
        var footer_position = footer.offset().top;

        if (!isSP) {
          if (fixed_position < footer_position + pointCheck) {
            fixed.removeClass("showbg");
          } else {
            fixed.addClass("showbg");
          }
        } else {
          if (window.pageYOffset > contents.offset().top + 200) {
            fixed.addClass("showbg");
          } else {
            fixed.removeClass("showbg");
          }
        }
      }
    }
    checkfixed();
    var scrollTimer, resizeTimer;
    $(window).on("scroll", function () {
      clearTimeout(scrollTimer);
      scrollTimer = setTimeout(function () {
        checkfixed();
      }, 0);
    });
    $(window).on("resize", function () {
      clearTimeout(resizeTimer);
      resizeTimer = setTimeout(function () {
        checkfixed();
      }, 16.67);
    });
  });
}
/* set height mv
-------------------------------------------------------------------------------------------------------------------- */
{
  let mainVisual = document.querySelector(".mainVisual");
  if (mainVisual) {
    window.addEventListener("load", function () {
      function setHeightResize() {
        if (isSP) {
          mainVisual.querySelector(".mainVisual-pic").style.height =
            mainVisual.offsetHeight + "px";
        } else {
          mainVisual.querySelector(".mainVisual-pic").style.height = "100%";
        }
      }
      setHeightResize();
      window.addEventListener(
        "pc2sp",
        (e) => {
          e.preventDefault();
          setHeightResize();
        },
        false
      );
      window.addEventListener(
        "sp2pc",
        (e) => {
          e.preventDefault();
          setHeightResize();
        },
        false
      );
    });
  }
}
/*set popup company
-------------------------------------------------------------------------------------------------------------------- */
{
  let checkbtn = document.querySelector(".btn-modal");
  if (checkbtn) {
    jQuery(function ($) {
      if ($(".btn-modal").length) {
        let $srcvideo;
        $(".btn-modal").on("click", function (e) {
          e.preventDefault();
          $srcvideo = $(this).attr("data-src");
          $("#video-modal-iframe").attr(
            "src",
            $srcvideo + "?autoplay=1&showinfo=0&modestbranding=1&rel=0&mute=0"
          );
          setTimeout(() => {
            $(".our-brand-modal").addClass("is-visible");
          }, 200);

          $(".our-brand-modal .overlay_popup").on("click", function (e) {
            e.preventDefault();
            $(".our-brand-modal").removeClass("is-visible");
            $("#video-modal-iframe").attr("src", $srcvideo);
          });
        });
      }
    });
  }
}
/*scroll achor link
-------------------------------------------------------------------------------------------------------------------- */
{
  function scrollOnPageLoad() {
    let offset;
    isSP ? (offset = 80) : (offset = 0);
    if (window.location.hash) window.scroll(0, 0);
    setTimeout(window.scroll(0, 0), 1);
    var hashLink = window.location.hash;
    if ($(hashLink).length > 0) {
      $("html, body")
        .stop()
        .animate(
          {
            scrollTop: $(hashLink).offset().top - offset,
          },
          180
        );
    }
  }
  $(window).on("load", function () {
    scrollOnPageLoad();
  });
}
/*tabs achor link
-------------------------------------------------------------------------------------------------------------------- */
{
  jQuery(document).ready(function ($) {
    $("#tabs-nav li:first-child").addClass("active");
    $(".tab-content").hide();
    $(".tab-content:first").show();

    $("#tabs-nav li").click(function () {
      $("#tabs-nav li").removeClass("active");
      $(this).addClass("active");
      $(".tab-content").hide();

      var activeTab = $(this).find("a").attr("href");
      $(activeTab).fadeIn();
      return false;
    });
  });
}
