"use strict";

// Load plugins
const autoprefixer = require("autoprefixer");
const browsersync = require("browser-sync").create();
const cssnano = require("cssnano");
const del = require("del");
const gulp = require("gulp");
const plumber = require("gulp-plumber");
const notify = require("gulp-notify");
const postcss = require("gulp-postcss");
const rename = require("gulp-rename");
const sass = require('gulp-sass')(require('sass'));
var sassGlob = require("gulp-sass-glob");
// var config = require("./config");

// Clean assets
function clean() {
  return del(["dist"]);
}
// CSS task
function css() {
  return gulp
    .src("scss/**/*.scss")
    .pipe(
      plumber({
        errorHandler: notify.onError(
          "Error on <gulp sass>: <%= error.message %>"
        ),
      })
    )
    .pipe(sassGlob())
    .pipe(sass({ outputStyle: "expanded" }))
    .pipe(postcss([autoprefixer(["> 100%"])]))
    .pipe(gulp.dest("./"))

    .pipe(browsersync ? browsersync.reload({ stream: true }) : noop());
}
// browser-sync
function server(done) {
  browsersync.init({
    watch: true,
    server: {
      baseDir: ".",
      index: "index.html",
    },
  });
  done();
}

// browser reload
function serverReload(done) {
  browsersync.reload();
  done();
}

// Watch files
function watchFiles() {
  gulp.watch("scss/**/*.scss", css, serverReload);
}

const build = gulp.series(clean, gulp.parallel(css));
const watch = gulp.parallel(watchFiles, server);

// export tasks
exports.css = css;
exports.clean = clean;
exports.build = build;
exports.watch = watch;
exports.default = gulp.series(clean, css, watch, server);
